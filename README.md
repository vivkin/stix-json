Overview
--------------
**StiX Json** is world's fastest(*) [json] parser with nice C++ API. It is based on modified [jsmn] lexer.

**StiX Json** pros:

* very fast;
* it is lightweight - only 4 files, totally around 800 lines of code;
* it does 0 allocations;
* it is open source under MIT license.

(*) according to http://lionet.livejournal.com/118853.html (russian)

Configuration
--------------
**StiX Json** has mandatory and optional configuration options. They are defined in **jsmn.h**

*Mandatory*:

* **JSMN_PARENT_LINKS** is required for links from child to parent;
* **JSMN_NEXT_LINKS** is required for links to next key node.

*Optional*:

* **JSMN_HUGE_FILES** is required for parsing huge files. If you will try to parse big files without this define assertion will pop up, so you'll know what to do :)

Performance
--------------
**StiX Json** was designed with performance and simplicity in mind. For best performance, you should compile *StiX Json* sources (including **jsmn** lexer) with **/Os (Favor small code)** VS flag. This will give you **33%** faster code.

Tests and Examples
--------------
Take a look at Test.cpp file for parser usage examples. There are 3 tests - *Test1*, *Test2* and **TestBig**.
First two demonstrate how to parse and navigate through Json file.
TestBig is dedicated to performance benchmarking. It compares parsing of 28Mb json file with **StiX Json**, **jsmn** lexer and **rapidjson**. In my personal tests **StiX Json** were **~3% faster** than original **jsmn** with /Ot (Favor fast code) and **~6% slower** with /Os (Favor small code).

#### Comparison table:

| /O2 Option | StiX Json | jsmn | rapidjson |
|------------|:---------:|:----:|:---------:|
| **/Ot** | 8.62 sec | 8.9 sec | 9.05 sec |
| **/Os** | 6.46 sec | 6.05 sec | 11 sec |

These are summary results of 100 loop parsing of 28Mb test file.

#####Test platform:
* Intel Core i5-3570K, 4.5GHz
* Visual Studio 2010
* Windows 7 64 bit



[json]: http://www.json.org/
[jsmn]: https://bitbucket.org/zserge/jsmn/overview