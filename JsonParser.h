
#pragma once

#include <cassert>
#include <cstring>
#include <cstdlib>
#include "jsmn/jsmn.h"


typedef unsigned int u32;

class JsonParser;
class JsonString;


struct ConstCharWrapper
{
	ConstCharWrapper(const char* str)
		: m_str(str)
	{
	}

	const char* m_str;
};



class JsonValue
{
public:
	jsmntype_t			GetType() const;

	JsonValue			GetValue() const;
	JsonValue			GetValue(ConstCharWrapper key) const;
	template <unsigned int N>
	JsonValue			GetValue(const char (&key)[N]) const;

	JsonValue			GetValueNonCaseSensetive(ConstCharWrapper key) const;
	template <unsigned int N>
	JsonValue			GetValueNonCaseSensetive(const char (&key)[N]) const;

	bool				IsValid() const;
	bool				IsKey() const;

	JsonString			AsString() const;
	double				AsNumber() const;
	bool				AsBool() const;
	bool				IsNull() const;

	u32					GetElementsCount() const;
	JsonValue			operator[](u32 index) const;

	JsonValue			Next() const;
	const JsonValue&	operator++();

protected:
private:
	friend class		JsonParser;

	jsmn_uint_t			m_internalTokenIndex;
	const JsonParser*	m_pParser;
	static JsonValue	JSON_INVALID_VALUE;

						JsonValue(jsmn_uint_t index, const JsonParser* pParser);

	template <class T>
	JsonValue			InternalGetValue(const char* key, size_t strLen) const;
};





class JsonParser
{
public:
	typedef void*		(*MemAlloc)(size_t);
	typedef void		(*MemFree)(void*);

						JsonParser();
						~JsonParser();

	static jsmnerr_t	CalcSpaceRequired(const char* pJsonString, size_t& result);

	jsmnerr_t			ParseJsonString(const char* pJsonString);
	jsmnerr_t			ParseJsonString(const char* pJsonString, void* pMem, size_t memSize);

	const JsonValue&	GetRoot() const;

	static void			SetMemFunctions(MemAlloc memAlloc, MemFree memFree);

protected:
private:
	friend class		JsonValue;
	friend class		JsonString;

	static MemAlloc		m_pMemAlloc;
	static MemFree		m_pMemFree;

	size_t				m_tokensCount;
	jsmntok*			m_pTokens;
	JsonValue			m_root;
	const char*			m_pJsonString;
	bool				m_bMemOwnership;

	const jsmntok*		InternalGetToken(u32 tokenIndex) const;
};




class JsonString
{
public:
	bool				IsEqualTo(ConstCharWrapper str) const;
	bool				IsEqualTo(ConstCharWrapper str, size_t strLen) const;
	template <unsigned int N>
	bool				IsEqualTo(const char (&str)[N]) const;

	bool				IsEqualToNonSensitive(ConstCharWrapper str) const;
	bool				IsEqualToNonSensitive(ConstCharWrapper str, size_t strLen) const;
	template <unsigned int N>
	bool				IsEqualToNonSensitive(const char (&str)[N]) const;

	u32					GetLength() const;

	void				ToCString(char* str, u32 size) const;

protected:
private:
	friend class		JsonValue;

	jsmn_uint_t			m_internalTokenIndex;
	const JsonParser*	m_pParser;

						JsonString(jsmn_uint_t index, const JsonParser* pParser);

	typedef int			(*StringComparator)(const char*, const char*, size_t);
	template <StringComparator F>
	bool				InternalIsJsonStrEqualTo(const char* str, size_t strLen) const;
};





struct JsonStrCmpWrapper 
{
	static bool			IsEqual(const JsonString& str1, const char* str2, size_t str2Len);
};

struct JsonStrCmpNonSensitiveWrapper 
{
	static bool			IsEqual(const JsonString& str1, const char* str2, size_t str2Len);
};





//-------------------------------------------------------------------------------------------
inline JsonParser::JsonParser()
	: m_tokensCount(0), m_pTokens(0), m_root(JsonValue::JSON_INVALID_VALUE), m_pJsonString(0), m_bMemOwnership(false)
{
}

//-------------------------------------------------------------------------------------------
inline JsonParser::~JsonParser()
{
	if (m_bMemOwnership)
	{
		(*m_pMemFree)(m_pTokens);
	}
}

//-------------------------------------------------------------------------------------------
inline jsmnerr_t JsonParser::CalcSpaceRequired(const char* pJsonString, size_t& result)
{
	jsmn_parser parser;
	jsmn_init(&parser);

	jsmnerr_t error = jsmn_pre_parse(&parser, pJsonString, &result);
	result *= sizeof(jsmntok_t);
	return error;
}

//-------------------------------------------------------------------------------------------
inline const JsonValue& JsonParser::GetRoot() const
{
	return m_root;
}

//-------------------------------------------------------------------------------------------
inline void JsonParser::SetMemFunctions(MemAlloc memAlloc, MemFree memFree)
{
	assert(m_pMemAlloc);
	assert(m_pMemFree);

	m_pMemAlloc = memAlloc;
	m_pMemFree = memFree;
}

//-------------------------------------------------------------------------------------------
inline const jsmntok* JsonParser::InternalGetToken(u32 tokenIndex) const
{
	assert(tokenIndex < m_tokensCount);
	return &m_pTokens[tokenIndex];
}





//-------------------------------------------------------------------------------------------
inline JsonValue::JsonValue(jsmn_uint_t index, const JsonParser* pParser)
	: m_internalTokenIndex(index), m_pParser(pParser)
{
}

//-------------------------------------------------------------------------------------------
inline jsmntype_t JsonValue::GetType() const
{
	assert(m_internalTokenIndex != JSMN_INVALID_VALUE);
	assert(m_pParser);

	return (jsmntype_t)m_pParser->InternalGetToken(m_internalTokenIndex)->type;
}

//-------------------------------------------------------------------------------------------
inline JsonValue JsonValue::GetValue() const
{
	assert(m_internalTokenIndex != JSMN_INVALID_VALUE);
	assert(m_pParser);
	assert(m_pParser->InternalGetToken(m_internalTokenIndex)->is_key);

	return JsonValue(m_internalTokenIndex + 1, m_pParser);
}

//-------------------------------------------------------------------------------------------
inline JsonValue JsonValue::GetValue(ConstCharWrapper key) const
{
	const size_t kStrLen = strlen(key.m_str);
	return InternalGetValue<JsonStrCmpWrapper>(key.m_str, kStrLen);
}

//-------------------------------------------------------------------------------------------
template <unsigned int N>
inline JsonValue JsonValue::GetValue(const char (&key)[N]) const
{
	return InternalGetValue<JsonStrCmpWrapper>(key, N - 1);
}

//-------------------------------------------------------------------------------------------
inline JsonValue JsonValue::GetValueNonCaseSensetive(ConstCharWrapper key) const
{
	const size_t kStrLen = strlen(key.m_str);
	return InternalGetValue<JsonStrCmpNonSensitiveWrapper>(key.m_str, kStrLen);
}

//-------------------------------------------------------------------------------------------
template <unsigned int N>
inline JsonValue JsonValue::GetValueNonCaseSensetive(const char (&key)[N]) const
{
	return InternalGetValue<JsonStrCmpNonSensitiveWrapper>(key, N - 1);
}

//-------------------------------------------------------------------------------------------
inline bool JsonValue::IsValid() const
{
	return (m_internalTokenIndex != JSMN_INVALID_VALUE) && (m_pParser != 0);
}

//-------------------------------------------------------------------------------------------
inline bool JsonValue::IsKey() const
{
	assert(m_internalTokenIndex != JSMN_INVALID_VALUE);
	assert(m_pParser);
	
	return m_pParser->InternalGetToken(m_internalTokenIndex)->is_key == 1;
}

//-------------------------------------------------------------------------------------------
inline JsonString JsonValue::AsString() const
{
	assert(m_internalTokenIndex != JSMN_INVALID_VALUE);
	assert(m_pParser);

	return JsonString(m_internalTokenIndex, m_pParser);
}

//-------------------------------------------------------------------------------------------
inline double JsonValue::AsNumber() const
{
	assert(m_internalTokenIndex != JSMN_INVALID_VALUE);
	assert(m_pParser);

	const jsmntok_t* kToken = m_pParser->InternalGetToken(m_internalTokenIndex);
	assert(kToken->type == JSMN_PRIMITIVE);

	return atof(&m_pParser->m_pJsonString[kToken->start]);
}

//-------------------------------------------------------------------------------------------
inline bool JsonValue::AsBool() const
{
	assert(m_internalTokenIndex != JSMN_INVALID_VALUE);
	assert(m_pParser);

	const jsmntok_t* kToken = m_pParser->InternalGetToken(m_internalTokenIndex);

	assert(kToken->type == JSMN_PRIMITIVE);
	assert(m_pParser->m_pJsonString[kToken->start] == 't' || m_pParser->m_pJsonString[kToken->start] == 'f');

	return m_pParser->m_pJsonString[kToken->start] == 't';
}

//-------------------------------------------------------------------------------------------
inline JsonValue JsonValue::Next() const
{
	assert(m_internalTokenIndex != JSMN_INVALID_VALUE);
	assert(m_pParser);

	return JsonValue(m_pParser->InternalGetToken(m_internalTokenIndex)->next, m_pParser);
}

//-------------------------------------------------------------------------------------------
inline const JsonValue& JsonValue::operator++()
{
	assert(m_internalTokenIndex != JSMN_INVALID_VALUE);
	assert(m_pParser);

	m_internalTokenIndex = m_pParser->InternalGetToken(m_internalTokenIndex)->next;
	return *this;
}

//-------------------------------------------------------------------------------------------
template <class T>
inline JsonValue JsonValue::InternalGetValue(const char* key, size_t strLen) const
{
	assert(m_internalTokenIndex != JSMN_INVALID_VALUE);
	assert(m_pParser);
	assert(m_pParser->InternalGetToken(m_internalTokenIndex)->type == JSMN_OBJECT);

	jsmn_uint_t tokenIndex = m_internalTokenIndex + 1;

	while (tokenIndex != JSMN_INVALID_VALUE)
	{
		JsonString str(tokenIndex, m_pParser);

		if (T::IsEqual(str, key, strLen))
		{
			return JsonValue(tokenIndex + 1, m_pParser);
		}

		tokenIndex = m_pParser->InternalGetToken(tokenIndex)->next;
	}

	return JSON_INVALID_VALUE;
}






//-------------------------------------------------------------------------------------------
inline JsonString::JsonString(jsmn_uint_t index, const JsonParser* pParser)
	: m_internalTokenIndex(index), m_pParser(pParser)
{
}

//-------------------------------------------------------------------------------------------
inline bool JsonString::IsEqualTo(ConstCharWrapper str) const
{
	const size_t kStrLen = strlen(str.m_str);
	return InternalIsJsonStrEqualTo<strncmp>(str.m_str, kStrLen);
}

//-------------------------------------------------------------------------------------------
inline bool JsonString::IsEqualTo(ConstCharWrapper str, size_t strLen) const
{
	return InternalIsJsonStrEqualTo<strncmp>(str.m_str, strLen);
}

//-------------------------------------------------------------------------------------------
template <unsigned int N>
inline bool JsonString::IsEqualTo(const char (&str)[N]) const
{
	return InternalIsJsonStrEqualTo<strncmp>(str, N - 1);
}

//-------------------------------------------------------------------------------------------
inline bool JsonString::IsEqualToNonSensitive(ConstCharWrapper str) const
{
	const size_t kStrLen = strlen(str.m_str);

	return InternalIsJsonStrEqualTo<
#ifdef _MSC_VER
	_strnicmp
#else
	strncasecmp
#endif
	>(str.m_str, kStrLen);
}

//-------------------------------------------------------------------------------------------
inline bool JsonString::IsEqualToNonSensitive(ConstCharWrapper str, size_t strLen) const
{
	return InternalIsJsonStrEqualTo<
#ifdef _MSC_VER
	_strnicmp
#else
	strncasecmp
#endif
	>(str.m_str, strLen);
}

//-------------------------------------------------------------------------------------------
template <unsigned int N>
inline bool JsonString::IsEqualToNonSensitive(const char (&str)[N]) const
{
	return InternalIsJsonStrEqualTo<
#ifdef _MSC_VER
	_strnicmp
#else
	strncasecmp
#endif
	>(str, N - 1);
}

//-------------------------------------------------------------------------------------------
inline u32 JsonString::GetLength() const
{
	const jsmntok_t* kToken = m_pParser->InternalGetToken(m_internalTokenIndex);

	return kToken->end - kToken->start;
}

//-------------------------------------------------------------------------------------------
template <JsonString::StringComparator F>
inline bool JsonString::InternalIsJsonStrEqualTo(const char* str, size_t strLen) const
{
	const jsmntok_t* kToken = m_pParser->InternalGetToken(m_internalTokenIndex);

	if ((jsmn_uint_t)strLen != kToken->end - kToken->start)
	{
		return false;
	}

	return !F(&m_pParser->m_pJsonString[kToken->start], str, strLen);
}





//-------------------------------------------------------------------------------------------
inline bool JsonStrCmpWrapper::IsEqual(const JsonString& str1, const char* str2, size_t str2Len)
{
	return str1.IsEqualTo(str2, str2Len);
}

//-------------------------------------------------------------------------------------------
inline bool JsonStrCmpNonSensitiveWrapper::IsEqual(const JsonString& str1, const char* str2, size_t str2Len)
{
	return str1.IsEqualToNonSensitive(str2, str2Len);
}