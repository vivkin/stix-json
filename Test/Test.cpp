
#include <cstdio>
#include <cstdlib>
#include <Windows.h>
#include "JsonParser.h"

//#define TEST_BIG

#ifdef TEST_BIG
#	include "jsmn_orig.h"
#	include "rapidjson/document.h"
#endif


double GetTime()
{
	__int64 time;
	__int64 freq;
	QueryPerformanceCounter((LARGE_INTEGER *)&time);  // Get current count
	QueryPerformanceFrequency((LARGE_INTEGER *)&freq); // Get processor freq
	return (double)(time)/(double)freq;
}

#ifndef TEST_BIG
void Test1()
{
	JsonParser parser;

	FILE* pFile = fopen("test.json", "rb");
	if (!pFile)
	{
		printf("failed to open file");
		return;
	}

	fseek(pFile, 0, SEEK_END);
	const int kFileSize = ftell(pFile);
	fseek(pFile, 0, SEEK_SET);

	char* pJsonString = (char*)malloc(kFileSize + 1);
	fread(pJsonString, kFileSize, 1, pFile);
	pJsonString[kFileSize] = '\0';	// parser expects null terminated string for parsing

	size_t memSize = 0;
	jsmnerr_t result = parser.CalcSpaceRequired(pJsonString, memSize);
	if (result != JSMN_SUCCESS)
	{
		printf("Json Error %d", result);
		return;
	}

	void* mem = malloc(memSize);

	if (parser.ParseJsonString(pJsonString, mem, memSize) != JSMN_SUCCESS)
	{
		printf("failed to parse string");
		return;
	}

	char str[64];

	JsonValue val = parser.GetRoot().GetValue("test");

	val.AsString().ToCString(str, 64);
	printf("%s\n", str);


	JsonValue root = parser.GetRoot();
	jsmntype_t type = root.GetType();
	u32 elements = root.GetElementsCount();

	JsonValue key = root[0].GetValue();
	elements = key.GetElementsCount();

	JsonValue innerKey = key[0];

	JsonString jStr = innerKey.GetValue().AsString();
	jStr.ToCString(str, 64);
	printf("%s\n", str);

	key = key.GetValue("GlossDiv");
	key = key.GetValueNonCaseSensetive("GlossList");

	JsonValue newKey = key.GetValueNonCaseSensetive("GlossDef");
	if (!newKey.IsValid())
	{
		printf("there are no key named \"GlossDef\" at \"GlossList\"\n");
	}

	key = key.GetValueNonCaseSensetive("GlossEntry");

	JsonString s = key.GetValueNonCaseSensetive("SortAs").AsString();
	s.ToCString(str, 64);
	printf("%s\n", str);

	s = key[2].AsString();
	s.ToCString(str, 64);
	printf("%s\n", str);

	key = key.GetValueNonCaseSensetive("glossdef");
	key = key.GetValueNonCaseSensetive("glossseealso");

	type = key.GetType();
	elements = key.GetElementsCount();

	for (u32 i = 0; i < elements; ++i)
	{
		if (key[i].GetType() == JSMN_STRING)
		{
			JsonString val = key[i].AsString();
			char str[64];
			val.ToCString(str, 64);
			printf("%s ", str);
		}
		else if (key[i].GetType() == JSMN_PRIMITIVE)
		{
			printf("%d ", (u32)key[i].AsNumber());
		}
		else if (key[i].GetType() == JSMN_ARRAY)
		{
			u32 elems = key[i].GetElementsCount();

			JsonValue array = key[i];

			for (u32 i = 0; i < elems; ++i)
			{
				JsonString val = array[i].AsString();
				char str[64];
				val.ToCString(str, 64);
				printf("%s ", str);
			}
		}
	}

	free(mem);
	free(pJsonString);
	fclose(pFile);
}

void Test2()
{
	JsonParser parser;

	FILE* pFile = fopen("json test.txt", "rb");
	if (!pFile)
	{
		printf("failed to open file");
		return;
	}

	fseek(pFile, 0, SEEK_END);
	const int kFileSize = ftell(pFile);
	fseek(pFile, 0, SEEK_SET);

	char* pJsonString = (char*)malloc(kFileSize + 1);
	fread(pJsonString, kFileSize, 1, pFile);
	pJsonString[kFileSize] = '\0';

	//JsonParser::SetMemFunctions(malloc, free);

	if (parser.ParseJsonString(pJsonString) != JSMN_SUCCESS)
	{
		printf("failed to parse string");
		return;
	}

	JsonValue value = parser.GetRoot().GetValue("web-app");	// get value for key "web-app" from root node. Note, that input string doesn't have to have root node - {}
	if (!value.IsValid())
	{
		return;
	}

	printf("\nkey %s type %d\n", "web-app", value.GetType());

	for (u32 i = 0; i < value.GetElementsCount(); ++i)
	{
		char str[64];
		JsonValue obj = value[i];

		for (u32 i = 0; i < obj.GetElementsCount(); ++i)
		{
			JsonValue key = obj[i];
			JsonString keyStr = key.AsString();
			keyStr.ToCString(str, 64);

			printf("%s : ", str);

			if (key.GetValue().GetType() == JSMN_STRING)
			{
				JsonString val = key.GetValue().AsString();
				val.ToCString(str, 64);

				printf("%s\n", str);
			}
		}
	}

	free(pJsonString);
	fclose(pFile);
}

#else

void TestBig()
{
	FILE* pFile = fopen("testBig.txt", "rb");
	if (!pFile)
	{
		printf("failed to open file");
		return;
	}

	fseek(pFile, 0, SEEK_END);
	const int kFileSize = ftell(pFile);
	fseek(pFile, 0, SEEK_SET);

	char* pJsonString = (char*)malloc(kFileSize + 1);
	fread(pJsonString, kFileSize, 1, pFile);
	pJsonString[kFileSize] = '\0';



	/************************************************************************/
	/*					jsmn parsing test                                   */
	/************************************************************************/

	jsmn::jsmn_parser parser;
	jsmn::jsmn_init(&parser);

	unsigned int tokensCount = jsmn::jsmn_pre_parse(&parser, pJsonString);
	jsmn::jsmntok_t* mem = (jsmn::jsmntok_t*)malloc(tokensCount * sizeof(jsmn::jsmntok_t));

	double time = GetTime();

	for (int i = 0; i < 100; ++i)
	{
		jsmn::jsmn_init(&parser);
		jsmn::jsmnerr_t error = jsmn::jsmn_parse(&parser, pJsonString, mem, tokensCount);
	}

	double newTime = GetTime();

	printf("jsmn delta t %f\n", newTime - time);

	free(mem);


	/************************************************************************/
	/*					StiX Json parsing test                              */
	/************************************************************************/

	JsonParser stixParser;

	size_t memSize = 0;
	if (stixParser.CalcSpaceRequired(pJsonString, memSize) != JSMN_SUCCESS)
	{
		printf("Json Error");
		return;
	}

	void* stixMem = malloc(memSize);

	time = GetTime();

	for (u32 i = 0; i < 100; ++i)
	{
		stixParser.ParseJsonString(pJsonString, stixMem, memSize);
	}

	newTime = GetTime();

	printf("StiXJson delta t %f\n", newTime - time);

	free(mem);


	/************************************************************************/
	/*					rapidjson parsing test                              */
	/************************************************************************/

	time = GetTime();

	for (u32 i = 0; i < 100; ++i)
	{
		rapidjson::Document document;
		document.Parse<0>(pJsonString);
	}

	newTime = GetTime();

	printf("rapidjson delta t %f\n", newTime - time);

	free(pJsonString);
	fclose(pFile);
}

#endif

int main()
{
#ifdef TEST_BIG
	TestBig();
#else
	Test1();

	Test2();
#endif

	return 0;
}