
#include "JsonParser.h"

JsonValue JsonValue::JSON_INVALID_VALUE(JSMN_INVALID_VALUE, 0);

JsonParser::MemAlloc JsonParser::m_pMemAlloc = malloc;
JsonParser::MemFree JsonParser::m_pMemFree = free;


//-------------------------------------------------------------------------------------------
jsmnerr_t JsonParser::ParseJsonString(const char* pJsonString)
{
	size_t size = 0;
	jsmnerr_t result = CalcSpaceRequired(pJsonString, size);
	if (result != JSMN_SUCCESS)
	{
		return result;
	}

	if (size > m_tokensCount * sizeof(jsmntok_t))
	{
		if (m_bMemOwnership)
		{
			(*m_pMemFree)(m_pTokens);
		}
	}
	
	void* mem = (*m_pMemAlloc)(size);
	m_bMemOwnership = true;

	return ParseJsonString(pJsonString, mem, size);
}

//-------------------------------------------------------------------------------------------
jsmnerr_t JsonParser::ParseJsonString(const char* pJsonString, void* pMem, size_t memSize)
{
	assert(pMem);

	m_pTokens = (jsmntok_t*)pMem;
	m_tokensCount = memSize / sizeof(jsmntok_t);
	m_pJsonString = pJsonString;
	m_root = JsonValue(0, this);

	jsmn_parser parser;
	jsmn_init(&parser);

	return jsmn_parse(&parser, pJsonString, m_pTokens, (u32)m_tokensCount);
}





//-------------------------------------------------------------------------------------------
bool JsonValue::IsNull() const
{
	assert(m_internalTokenIndex != JSMN_INVALID_VALUE);
	assert(m_pParser);

	const jsmntok_t* kToken = m_pParser->InternalGetToken(m_internalTokenIndex);

	if (kToken->type == JSMN_PRIMITIVE)
	{
		return m_pParser->m_pJsonString[kToken->start] == 'n';
	}

	return false;
}

//-------------------------------------------------------------------------------------------
u32 JsonValue::GetElementsCount() const
{
	assert(m_internalTokenIndex != JSMN_INVALID_VALUE);
	assert(m_pParser);

	const jsmntok_t* kToken = m_pParser->InternalGetToken(m_internalTokenIndex);
	assert(kToken->type == JSMN_OBJECT || kToken->type == JSMN_ARRAY);

	if (kToken->type == JSMN_OBJECT)
	{
		return kToken->size / 2;	// both keys and values are counted as an elements of Object token
	}

	return kToken->size;
}

//-------------------------------------------------------------------------------------------
JsonValue JsonValue::operator[](u32 index) const
{
	assert(index < GetElementsCount());
	assert(m_internalTokenIndex != JSMN_INVALID_VALUE);
	assert(m_pParser);
	assert(m_pParser->InternalGetToken(m_internalTokenIndex)->type == JSMN_OBJECT || m_pParser->InternalGetToken(m_internalTokenIndex)->type == JSMN_ARRAY);

	jsmn_uint_t nextIndex = m_internalTokenIndex + 1;

	for (u32 i = 0; i < index; ++i)
	{
		const jsmntok_t* kToken = m_pParser->InternalGetToken(nextIndex);
		nextIndex = kToken->next;
	}

	return JsonValue(nextIndex, m_pParser);
}





//-------------------------------------------------------------------------------------------
void JsonString::ToCString(char* str, u32 size) const
{
	const jsmntok_t* kToken = m_pParser->InternalGetToken(m_internalTokenIndex);
	const u32 kSize = kToken->end - kToken->start;
	assert(size >= kSize + 1);

	memcpy(str, &m_pParser->m_pJsonString[kToken->start], kSize);
	str[kSize] = '\0';
}